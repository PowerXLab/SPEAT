% Function: generate_signal_interference
% Generates various test signals for anti-interference test
% Input: the interference flag
% Output: the test signal

function [Signal] = generate_signal_interference(tag)

f0 = evalin('base','f0');
fs = evalin('base','fs');

sampling_number = fs / f0;   % the number of sampling points of a cycle
fs=sampling_number * 50;   % sampling frequency
Ts = 1 / fs;   % sampling period

time_con = 5*sampling_number;   % time constant of DDC
n_beforefault = 1 : 3*sampling_number-1;   % signal sampling point before fault
n_afterfault = 3*sampling_number : 15*sampling_number-1;   % signal sampling point after fault

% generate test signal
Signal_beforefault = 0.1 * cos(2*pi*50*n_beforefault*Ts - pi/3);


% The corresponding signal is generated according to the tag
switch tag

    case 1   % 1. Single DDC test
        
        % obtains magnitude and time constant of primary DDC from the basic workspace
        fundamental_mag = evalin('base','fundamental_mag');
        fundamental_pha = evalin('base','fundamental_pha');
        DDC_mag = evalin('base','DDC_mag');
        DDC_time_con = evalin('base','DDC_time_con');
        % converts the time constant to a sampling point representation
        % 100 ms convert to 5*sampling_number
        time_con_tans = 5*sampling_number / 100 * DDC_time_con;
        Signal_afterfault = fundamental_mag*cos(2*pi*f0*n_afterfault*Ts +fundamental_pha)...
                + DDC_mag * exp(-(n_afterfault-3*sampling_number) / time_con_tans);
        Signal = [Signal_beforefault, Signal_afterfault];
        return;


    case 2   % 2. multiple DDCs test
        
        % obtains magnitude and time constant of primary DDC from the basic workspace
        fundamental_mag = evalin('base','fundamental_mag');
        fundamental_pha = evalin('base','fundamental_pha');
        DDC_mag_pri = evalin('base', 'DDC_mag_pri');
        DDC_time_con_pri = evalin('base', 'DDC_time_con_pri');
        % converts the time constant to a sampling point representation
        time_con_tans_pri = 5 * sampling_number / 100 * DDC_time_con_pri;
        % obtains magnitude and time constant of primary DDC from the basic workspace
        DDC_mag_sub = evalin('base', 'DDC_mag_sub');
        DDC_time_con_sub = evalin('base', 'DDC_time_con_sub');
        time_con_tans_sub = 5 * sampling_number / 100 * DDC_time_con_sub;   % convert time constant

        Signal_afterfault = fundamental_mag*cos(2*pi*f0*n_afterfault*Ts +fundamental_pha)...
                + DDC_mag_pri * exp(-(n_afterfault-3*sampling_number) / time_con_tans_pri)...
                + DDC_mag_sub * exp(-(n_afterfault-3*sampling_number) / time_con_tans_sub);
        Signal = [Signal_beforefault, Signal_afterfault];
        return;


    case 3   % 3. noise test
        
        fundamental_mag = evalin('base','fundamental_mag');
        fundamental_pha = evalin('base','fundamental_pha');
        DDC_mag = evalin('base','DDC_mag');
        DDC_time_con = evalin('base','DDC_time_con');
        snr = evalin('base', 'snr');
        % converts the time constant to a sampling point representation
        % 100 ms convert to 5*sampling_number
        time_con_tans = 5*sampling_number / 100 * DDC_time_con;
        Signal_afterfault = fundamental_mag*cos(2*pi*f0*n_afterfault*Ts +fundamental_pha)...
            + DDC_mag * exp(-(n_afterfault-3*sampling_number) / time_con_tans);  
        Signal_afterfault = awgn(Signal_afterfault, snr, 'measured');
        Signal = [Signal_beforefault, Signal_afterfault];
        return;


    case 4   % 4. harmonic test
        
        fundamental_mag = evalin('base','fundamental_mag');
        fundamental_pha = evalin('base','fundamental_pha');
        DDC_mag = evalin('base','DDC_mag');
        DDC_time_con = evalin('base','DDC_time_con');
        % converts the time constant to a sampling point representation
        % 100 ms convert to 5*sampling_number
        time_con_tans = 5*sampling_number / 100 * DDC_time_con;

        % random generation of harmonic order and phase
        har_order1 = randi([2 10], 1, 3);
        har_order2 = randi([11 16], 1, 3);
        har_phase = unifrnd(0, 2*pi, 1, 6);

        Signal_afterfault = fundamental_mag*cos(2*pi*f0*n_afterfault*Ts +fundamental_pha)...
            + DDC_mag * exp(-(n_afterfault-3*sampling_number) / time_con_tans)...
            + 0.15*cos(2*pi*har_order1(1)*f0*n_afterfault*Ts+har_phase(1))...
            + 0.15*cos(2*pi*har_order1(2)*f0*n_afterfault*Ts + har_phase(2))...
            + 0.15*cos(2*pi*har_order1(3)*f0*n_afterfault*Ts + har_phase(3))...
            + 0.07*cos(2*pi*har_order2(1)*f0*n_afterfault*Ts + har_phase(4))...
            + 0.07*cos(2*pi*har_order2(2)*f0*n_afterfault*Ts + har_phase(5))...
            + 0.07*cos(2*pi*har_order2(3)*f0*n_afterfault*Ts + har_phase(6));
        Signal = [Signal_beforefault, Signal_afterfault];
        return;

    case 5   % 5. inter-harmonic test

        fundamental_mag = evalin('base','fundamental_mag');
        fundamental_pha = evalin('base','fundamental_pha');
        DDC_mag = evalin('base','DDC_mag');
        DDC_time_con = evalin('base','DDC_time_con');
        % converts the time constant to a sampling point representation
        % 100 ms convert to 5*sampling_number
        time_con_tans = 5*sampling_number / 100 * DDC_time_con;

        min_fre = evalin('base','min_fre');
        max_fre = evalin('base','max_fre');
        % generating random interharmonic orders
        inter_fre = randi([min_fre, max_fre], 1);
        % check and adjust the interval
        % the difference between any two orders should be greater than
        % interharmonic orders cannot be a multiple of 50
        for i = 2:10
            value = randi([min_fre, max_fre], 1);
            while any(abs(inter_fre - value) < 1) || mod(value, 50) == 0
                value = randi([min_fre, max_fre], 1);
            end
            inter_fre(i) = value;
        end
        inter_mag = unifrnd(0, 0.2, 1,10);
        inter_phase = unifrnd(0, 2*pi, 1, 10);

        Signal_afterfault = fundamental_mag*cos(2*pi*f0*n_afterfault*Ts +fundamental_pha)...
            + DDC_mag * exp(-(n_afterfault-3*sampling_number) / time_con_tans);
        % generate fault currents containing interharmonics
        for inter_order = 1 : length(inter_fre)
            Signal_afterfault = Signal_afterfault + inter_mag(inter_order) * cos(2*pi...
                *inter_fre(inter_order) * n_afterfault*Ts + inter_phase(inter_order));
        end
        Signal = [Signal_beforefault, Signal_afterfault];
        return;


    case 6   % 6. signal aliasing
        fundamental_mag = evalin('base','fundamental_mag');
        fundamental_pha = evalin('base','fundamental_pha');
        aliasing_mag = evalin('base','aliasing_mag');
        aliasing_phase = evalin('base','aliasing_phase');
        aliasing_fre = evalin('base','aliasing_fre');
        Signal_afterfault = fundamental_mag*cos(2*pi*f0*n_afterfault*Ts + fundamental_pha) ...
            + 1*exp(-(n_afterfault-3*sampling_number) / time_con)...
            + aliasing_mag * cos(2*pi*aliasing_fre*n_afterfault*Ts + aliasing_phase);
        Signal = [Signal_beforefault, Signal_afterfault];
        return;
        
    otherwise
        return;
end

end
