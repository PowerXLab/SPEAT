% Function: execute_algorithm_main
% The purpose of this function is to execute the selected algorithms
% Input: app, test signal, the selected algorithms, the number of selected algorithms
% Output: the phasor, sampling number beyond one cycle


function [Phasor,sampling_number_beyond] = execute_algorithm_f0(app,test_signal,Alg_selec,num_alg_selec)

f0 = evalin('base','f0');
fs = evalin('base','fs');
color_set = evalin('base','color_set');

sampling_number = fs / f0;
% execute the algorithm and draw the image
Phasor=cell(num_alg_selec,2); % num_alg_selec*2 The first column is the estimated amplitude and the second column is the estimated phase angle
sampling_number_beyond=cell(num_alg_selec,1); % The algorithm uses additional sampling points on the basis of one periodic sampling points

for num=1:num_alg_selec
    [Phasor{num,1},Phasor{num,2},sampling_number_beyond{num,1}]=eval([Alg_selec{num},'(test_signal);']);
    hold(app.magnitude_figure2,'on');
    plot(app.magnitude_figure2,Phasor{num,1}(3*sampling_number-sampling_number_beyond{num,1}+1:7*sampling_number-...
        sampling_number_beyond{num,1}),'color',color_set{num},'LineWidth',2);
end

for num=1:num_alg_selec
    hold(app.phase_figure2,'on');
    plot(app.phase_figure2,Phasor{num,2}(3*sampling_number-sampling_number_beyond{num,1}+1:7*sampling_number...
        -sampling_number_beyond{num,1}),'color',color_set{num},'LineWidth',2);

end


