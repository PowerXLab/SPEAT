% Function: clear_figure_table
% The purpose of this function is to clear all images and tables
% Input: the app
% Output: no output

function clear_figure_table(app)

cla(app.magnitude_figure1);
cla(app.phase_figure1);
cla(app.test_signal_figure1);
cla(app.magnitude_figure2);
cla(app.phase_figure2);
cla(app.test_signal_figure2);
cla(app.magnitude_figure3);
cla(app.phase_figure3);
cla(app.test_signal_figure3);
app.max_error_table1.Data = [];
app.max_error_table2.Data = [];
app.Dynamic_table.Data = [];

end

