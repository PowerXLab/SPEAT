% Function: display_legend
% The purpose of this function is to diaplay the legend
% Input: app, the name of the selected algorithm, the selected algorithm
% Output: no output

function display_legend(app, num_alg_selec, Alg_selec)

color_text_show=cell(num_alg_selec,1);
color_show = cell(num_alg_selec,1);
color_text_show_text = cell(num_alg_selec,1);
color_text_show_visible = cell(num_alg_selec,1);
color_show_visible = cell(num_alg_selec,1);

for num=1:num_alg_selec

    color_text_show{num}=strcat('app.Color_text',string(num)); % write the selected algorithm name legend
    color_show{num}=strcat('app.Color',string(num)); % write the color legend
 
    % app.Color_text1.Text=app.Alg_selec{num}
    color_text_show_text{num} = strcat(color_text_show{num},'.Text');
    eval(strcat(color_text_show_text{num}, '= Alg_selec{num}'));

    % app.Color_text1.Visible="on"
    color_text_show_visible{num} = strcat(color_text_show{num},'.Visible');
    eval(strcat(color_text_show_visible{num}, '= "on"'));
    
    % app.Color1.Visible="on"
    color_show_visible{num} = strcat(color_show{num},'.Visible');
    eval(strcat(color_show_visible{num}, '= "on"'));
end

end

