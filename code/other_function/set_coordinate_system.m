% Function: set_coordinate_system
% The purpose of this function is to set coordinate interval
% Input: the app
% Output: no output


function set_coordinate_system(app)

f0 = evalin('base','f0');
fs = evalin('base','fs');
sampling_number = fs / f0;

app.magnitude_figure1.XTick=[0 sampling_number 2*sampling_number...
    3*sampling_number 4*sampling_number 5*sampling_number];
app.magnitude_figure1.XTickLabel=[0 20 40 60 80 100];
app.phase_figure1.XTick=[0 sampling_number 2*sampling_number...
    3*sampling_number 4*sampling_number 5*sampling_number];
app.phase_figure1.XTickLabel=[0 20 40 60 80 100];
app.magnitude_figure1.XLabel.String='Time (ms)';
app.magnitude_figure1.YLabel.String='Magnitude (p.u.)';
app.phase_figure1.XLabel.String='Time (ms)';
app.phase_figure1.YLabel.String='Phase (p.u.)';

end

