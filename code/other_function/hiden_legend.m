% Function: hiden_legend
% The purpose of this function is to hiden the legend
% Input: app, the name of the selected algorithm
% Output: no output

function hiden_legend(app, num_alg_selec)

color_text_show = cell(num_alg_selec,1); % traverse algorithm name legend
color_show = cell(num_alg_selec,1); % traverse color legend
color_text_show_visible = cell(num_alg_selec,1);
color_show_visible = cell(num_alg_selec,1);

for num = 1:5
    
    color_text_show{num} = strcat('app.Color_text',string(num)); % write the algorithm name legend
    color_show{num} = strcat('app.Color',string(num)); % write the color legend

    % app.Color1.Visible = "off"
    color_show_visible{num} = strcat(color_show{num},'.Visible');
    eval(strcat(color_show_visible{num},'= "off"')); 

    % app.Color_text1.Visible = "off"
    color_text_show_visible{num} = strcat(color_text_show{num},'.Visible');
    eval(strcat(color_text_show_visible{num},'= "off"')); 

end

end

