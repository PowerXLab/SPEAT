% Function: save_algorithm
% The purpose of this function is to savesthe imported algorithm to the specified folder
% Input: the name of the imported algorithm, storage path, path of the algorithm to be imported
% Output: no output

function save_algorithm(name, savefloder, file_tobe_improt)

savename=name;

% The file must be saved in .m format
savename=strcat(savename,'.m');
savepath=fullfile(savefloder,savename);
copyfile(file_tobe_improt, savepath);

end

