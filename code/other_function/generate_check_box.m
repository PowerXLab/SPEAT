% Function: generate_check_box
% The purpose of this function is to generate the corresponding check box
% after the algorithm is imported
% Input: app, the name of the imported algorithm, the number of previously imported algorithms
% Input: check box text size
% Output: the new number of previously imported algorithms


function num_alg_import_ago = generate_check_box(app, name, num_alg_import, fontSize)

% generate check box

sequence = 6 + num_alg_import;
% use the grid index to get the desired check boxes
checkbox = app.GridLayout.Children(sequence);
checkbox.FontSize = fontSize;
checkbox.Text = name;
checkbox.Visible = "on";

% the new number of previously imported algorithms is equal to 
% the number of algorithms imported so far
num_alg_import_ago = num_alg_import;

end


