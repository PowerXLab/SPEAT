% Function: check_algorithm_select
% If the user does not click "submit", a prompt is given
% Input: whether to click the "submit" sign
% Output: no output


function check_submit(is_submit)

if is_submit==0 % not click Submit

    dlg = warndlg('Please click the Submit button first','warning');   % creates a warning dialog with the specified message
    dlgChildren = get(dlg, 'Children');   % retrieves the child component of the dialog box
    msgText = findall(dlgChildren, 'Type', 'Text');   % finds all text components in the dialog box
    set(msgText, 'FontSize',10);   % set the font size of the text component to 10
    dlgButton = findobj(dlg, 'Type', 'UIControl');   % finds UI control components in a dialog box, including buttons
    set(dlgButton, 'String', 'OK');

end

end

