% Function: check_algorithm_select
% Check the selected algorithm and quantity
% Input: the app
% Output: the number of algorithms selected, the mane of algorithms selected


function [num_alg_select,name_alg_select] = check_algorithm_select(app)

name_alg_select = {};
checkBox = app.GridLayout.Children;
num_alg_select = 0;

% traverse the algorithm check box
for num_checkBox = 1:numel(checkBox)
    
    % if the algorithm is selected, number of selected algorithm is added by one and record the name
    if checkBox(num_checkBox).Value == 1 

        num_alg_select = num_alg_select + 1;
        name_alg_select{end+1} = checkBox(num_checkBox).Text;

    end

end

end

