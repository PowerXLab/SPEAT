# SPEAT

#### Description
开源了一个同步相量测量算法抗DDC相量测量算法综合测试工具（Synchronous Phasor Estimation Algorithm Testing，SPEAT）。实现了基于全波傅里叶变换、半波傅里叶变换、最小二乘与Prony的相量测量算法，建立了包含抗干扰性测试、敏感度测试、动态性能测试与综合评价方法的算法性能综合评估方案。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
